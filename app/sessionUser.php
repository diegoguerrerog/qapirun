<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sessionUser extends Model
{

    public $table = "sessionuser";

    protected $fillable = ['user_id', 'time', 'distancia', 'tipo', 'descripcion'];

   
}
