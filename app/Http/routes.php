<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});*/
Route::group(['middleware' => 'cors'],function(){
	Route::post('/auth_login','ApiAuthController@userAuth');
});
Route::post('/register','userController@register');
Route::post('/mail','userController@maill');
Route::put('/profile','userController@profile');
Route::post('/profilesearch','userController@profilesearch');
Route::post('/sessionsave','sessionController@save');
Route::post('/sessionslist','sessionController@sessionslist');

