<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiController;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Illuminate\Http\Request;
use App\User;
//use Request;
class ApiAuthController extends Controller
{
    public function userAuth(Request $request){
    	$credentials = $request->only('email','password');
    	$token = null;
        if(isset($credentials["email"])){
            $credentials = $request->only('email','password');
            $valores = $credentials;
        }else{
            $valores = $request->all();
            $valores = key($valores);
            $valores = json_decode($valores,true);
            $valores["email"] = str_replace("_", ".", $valores["email"]);
            $credentials = $valores;
        }
       
    	try {
    		if(!$token = JWTAuth::attempt($credentials)){
    			return response()->json(['error'=>'invalid_credentials']);
    		}
    	} catch (JWTException $ex) {
    		return response()-json(['error'=>'something_went_wrong'],500);
    	}
        $user = JWTAuth::toUser($token);

    	return response()->json(compact('token','user'));
    }
     
}
