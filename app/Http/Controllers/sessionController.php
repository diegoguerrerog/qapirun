<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\sessionUser;
use Illuminate\Support\Facades\DB;

class sessionController extends Controller
{
    
    public function save(Request $request){

        $data = $request->json()->all();
            $rules = [
                      'distancia' => 'required',
                      'tipo' => 'required',
                      'descripcion' => 'required',
                      'time' => 'required',
                      'user_id' => 'required'
                  ];

            $validator = \Validator::make($data["data"], $rules);
              if ($validator->fails()) {
                  return [
                      'created' => false,
                      'errors' => $validator->errors()->all()
                  ];
              }
            sessionUser::create($data["data"]);
            return \Response::json(['saved' => true], 200); 
    }

    public function sessionslist(Request $request){
        $data = $request->json()->all();
        $sessionUser = sessionUser::where('user_id',$data["data"])->get();
        return $sessionUser;
    }

    
}
