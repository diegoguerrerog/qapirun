<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ApiController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Mail;
//use Request;
class userController extends Controller
{
    
    public function register(Request $request){


        $data = $request->json()->all();
        $credentials = $data;
        
        $alld = DB::table('users')->where("email",$credentials["email"])->orderBy('id', 'desc')->first();
        if(isset($alld)){ //validacion si tabla esta vacia
            return "Email is already registered";
        }else{
            //$credentials["password"] = \Illuminate\Support\Facades\Hash::make($credentials["password"]);
            try{
                $email33 = (string)$credentials["email"];
                $users = [
                    [
                        'name' => $credentials["name"],
                        'email' => $credentials["email"],
                        'password' => Hash::make($credentials["password"])
                    ]
                ];
               // echo $users[0]["email"];
               // return ;
                foreach ($users as $user){
                    \App\User::create($user);
                }
               // User::create($credentials);

                Mail::send('email.register',['emailto' => $email33,"user"=>$credentials["email"],"clave"=>$credentials["password"]],function($mensaje) use ($email33){
                    $mensaje->subject("Correo de registro");
                    $mensaje->to($email33);
                });


                return "It was successfully registered";
            } catch (Exception $e) {
                \Log::info('Hubo un error, intente de nuevo: ' . $e);
                return \Response::json(['created' => false], 500);
            }
            
       
        }
    }
    public function maill(){
        Mail::send('email.register',["probando"=>'prueba'],function($mensaje){
            $mensaje->subject("Correo de registro");
            $mensaje->to("zorro8815@gmail.com");
        });
    }

    public function profile(Request $request){
        $data = $request->json()->all();
        
        try{
            $rules = [
                      'email' => 'required|email',
                      'lastname' => 'required',
                      'age' => 'required',
                      'firstname' => 'required',
                      'gender' => 'required',
                      'file' => 'required'
                  ];

            $validator = \Validator::make($data["data"], $rules);
              if ($validator->fails()) {
                  return [
                      'created' => false,
                      'errors' => $validator->errors()->all()
                  ];
              }

            $datauser = User::where('email',$data["data"]["email"])->get();
            if (!$datauser->isEmpty()){

                $user = User::where('email',$data["data"]["email"])
                    ->update(['lastname' => $data["data"]["lastname"],
                        'age' => $data["data"]["age"],
                        'name' => $data["data"]["firstname"],
                        'gender' => $data["data"]["gender"],
                        'photo' => $data["data"]["file"]
                        ]);
                return \Response::json(['update' => true], 200);
            }else            
                return \Response::json(['error' =>"No existe email"], 200);
       
        }catch (\Exception $e) {           
            return \Response::json(['update' => false], 500);
        }
    }

    public function profilesearch(Request $request){
        $data = $request->json()->all();
        $datauser = User::where('email',$data["data"]["email"])->get();
        return $datauser;
    }
}
