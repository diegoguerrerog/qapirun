<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
        	[
	        	'name' => 'Diego Guerrero1',
	        	'email' => 'zorro8815@gmail.com1',
	        	'password' => Hash::make('123456789')
        	],
            [
                'name' => 'Diego Guerrero2',
                'email' => 'a@a.com',
                'password' => Hash::make('a')
            ]
        ];

        foreach ($users as $user){
        	\App\User::create($user);
        }
    }
}
